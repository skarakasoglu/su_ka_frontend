package models

type ResetPassword struct{
	Base
	Username string
	Token string
}
