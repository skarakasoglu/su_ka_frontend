package models

type Profile struct{
	ID int
	Username string
	FullName string
	Address string
	Telephone string
	IsAppAuth bool
	ErrorMessage string
}
