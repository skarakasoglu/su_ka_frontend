package models

type AuthTypeHeader string

const (
	AuthTypeHeader_App    AuthTypeHeader = "Authorization"
	AuthTypeHeader_Google                = "X-Google-Authorization"
)

type AuthInfo struct{
	Token  string
	UserID int
	Type   AuthTypeHeader
}
