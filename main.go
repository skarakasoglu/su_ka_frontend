package main

import (
	"fmt"
	"golang-challenge/frontend/server"
	"log"
	"os"
)

func main() {
	log.Printf("Starting the Golang Challenge frontend server.")

	srvConf := server.Config{
		Address:  fmt.Sprintf(":%v", os.Getenv("PORT")),
		ViewPath: os.Getenv("VIEW_PATH"),
		StaticFilePath: os.Getenv("STATIC_FILE_PATH"),
		ApiURL:   "https://golang-challenge-backend.herokuapp.com/api/v1",
	}

	googleConf := server.GoogleAuthConfig{
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		RedirectURL:  "https://golang-challenge-frontend.herokuapp.com/google/auth",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	}

	srv := server.NewServer(srvConf, googleConf)
	err := srv.Start()
	if err != nil {
		log.Printf("Error on starting the server: %v", err)
	}
}