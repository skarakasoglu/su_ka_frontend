package responses

type BaseResponse struct{
	Code int `json:"code"`
	ErrorMessage string `json:"error_message"`
	SuccessMessage string `json:"success_message"`
}
