package responses

type LoginResponse struct{
	BaseResponse `json:"status"`
	AccessToken string `json:"access_token"`
	UserID int `json:"user_id"`
}
