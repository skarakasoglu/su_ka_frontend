package server

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"golang-challenge/frontend/models"
	"golang-challenge/frontend/server/requests"
	"golang-challenge/frontend/server/responses"
	"io"
	"io/ioutil"
	"net/http"
)

type apiClient struct{
	apiURL string
}

func NewApiClient(apiURL string) *apiClient{
	return &apiClient{
		apiURL: apiURL,
	}
}

func (api *apiClient) SignUp(username string, password string, authType int) (responses.BaseResponse, error) {
	var result responses.BaseResponse
	request := requests.SignUpRequest{
		Username: username,
		Password: password,
		AuthType: authType,
	}

	buf, err := json.Marshal(request)
	if err != nil {
		return result, err
	}

	signupApiURL := fmt.Sprintf("%v/signup", api.apiURL)
	resp, err := api.makeRequest(http.MethodPost, signupApiURL, bytes.NewBuffer(buf), nil)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(buffer, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (api *apiClient) ForgotPassword(username string) (responses.BaseResponse, error) {
	var result responses.BaseResponse

	forgotPasswordURL := fmt.Sprintf("%v/password/forgot/%v", api.apiURL, username)
	resp, err := api.makeRequest(http.MethodGet, forgotPasswordURL, nil, nil)
	if err != nil {
		return result, nil
	}
	defer resp.Body.Close()

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(buf, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (api *apiClient) ResetPassword(username string, resetToken string, password string) (responses.BaseResponse, error) {
	var result responses.BaseResponse
	request := requests.ResetPasswordRequest{
		Username:    username,
		ResetToken:  resetToken,
		NewPassword: password,
	}

	buf, err := json.Marshal(request)
	if err != nil {
		return result, err
	}

	resetPasswordURL := fmt.Sprintf("%v/password/reset", api.apiURL)
	resp, err := api.makeRequest(http.MethodPost, resetPasswordURL, bytes.NewBuffer(buf), nil)
	defer resp.Body.Close()

	buffer ,err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(buffer, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (api *apiClient) EditUser(tokenType models.AuthTypeHeader, token string, userID int, username string,
	fullName string, address string, telephone string) (responses.BaseResponse, error) {
	var result responses.BaseResponse

	editProfileRequest := requests.EditUserRequest{
		UserID:    userID,
		Username:  username,
		FullName:  fullName,
		Address:   address,
		Telephone: telephone,
	}

	buf, err := json.Marshal(editProfileRequest)
	if err != nil {
		return result, err
	}

	headers := map[string]string{string(tokenType): token }
	editProfileURL := fmt.Sprintf("%v/user", api.apiURL)
	resp, err := api.makeRequest(http.MethodPut, editProfileURL, bytes.NewBuffer(buf), headers)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(buffer, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}


func (api *apiClient) GetUserInfo(authInfo models.AuthInfo) (responses.GetUserResponse, error) {
	var result responses.GetUserResponse
	headers := map[string]string{ string(authInfo.Type): authInfo.Token }

	getUserURL := fmt.Sprintf("%v/user/%v", api.apiURL, authInfo.UserID)
	resp, err := api.makeRequest(http.MethodGet, getUserURL,nil, headers)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	var response responses.GetUserResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return result, err
	}

	result = response
	return result, nil
}

func (api *apiClient) SignIn(username string, password string, authType AuthType) (responses.LoginResponse, error) {
	var result responses.LoginResponse
	var request requests.LoginRequest

	request.Username = username
	request.Password = base64.StdEncoding.EncodeToString([]byte(password))
	request.AuthType = int(authType)

	buffer, err  := json.Marshal(request)
	if err != nil {
		return result, err
	}

	requestUrl := fmt.Sprintf("%v/login", api.apiURL)
	resp, err := api.makeRequest(http.MethodPost, requestUrl, bytes.NewBuffer(buffer), nil)
	if err != nil {
		return result, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (api *apiClient) makeRequest(method string, url string, body io.Reader, headers map[string]string) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return resp, err
	}

	return resp, err
}