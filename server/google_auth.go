package server

import (
	"context"
	"github.com/gorilla/sessions"
	"golang-challenge/frontend/models"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"log"
	"net/http"
	"net/url"
	"strings"
)

type GoogleAuthConfig struct{
	ClientID string
	ClientSecret string
	RedirectURL string
	Scopes []string
}

const (
	stateSignup = "state_sign_up"
	stateSignin = "state_sign_in"
)

type googleAuthHandler struct{
	config *oauth2.Config
	cookieStore *sessions.CookieStore
	apiClient *apiClient
}

func NewGoogleAuthHandler(authCfg GoogleAuthConfig, cookieStore *sessions.CookieStore , client *apiClient) *googleAuthHandler {
	return &googleAuthHandler{
		config:       &oauth2.Config{
			ClientID:     authCfg.ClientID,
			ClientSecret: authCfg.ClientSecret,
			Endpoint:     google.Endpoint,
			RedirectURL:  authCfg.RedirectURL,
			Scopes:       authCfg.Scopes,
		},
		cookieStore: cookieStore,
		apiClient: client,
	}
}

func (gah *googleAuthHandler) SignInWithGoogle(w http.ResponseWriter, r *http.Request) {
	log.Printf("Login with google")

	state := r.URL.Query().Get("state")

	URL, err := url.Parse(gah.config.Endpoint.AuthURL)
	if err != nil {
		log.Printf("Error on parsing the url: %v" + err.Error())
	}
	log.Print(URL.String())
	parameters := url.Values{}
	parameters.Add("client_id", gah.config.ClientID)
	parameters.Add("scope", strings.Join(gah.config.Scopes, " "))
	parameters.Add("redirect_uri", gah.config.RedirectURL)
	parameters.Add("response_type", "code")
	parameters.Add("state",  state)
	URL.RawQuery = parameters.Encode()
	url := URL.String()
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (gah *googleAuthHandler) SignInCallback(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")
	if state != stateSignup && state != stateSignin {
		log.Print("invalid oauth state, expected " + stateSignin + " or " + stateSignup + ", got " + state + "\n")
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	log.Print(code)

	if code == "" {
		log.Print("Code not found..")
		w.Write([]byte("Code Not Found to provide AccessToken..\n"))
		reason := r.FormValue("error_reason")
		if reason == "user_denied" {
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		// User has denied access..
		// http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	} else {
		token, err := gah.config.Exchange(context.Background(), code)
		if err != nil {
			log.Print("oauthConfGl.Exchange() failed with " + err.Error() + "\n")
			return
		}

		if state == stateSignup {
			resp, err := gah.apiClient.SignUp("", token.AccessToken, 2)
			if err != nil {
				log.Printf("Error on sign up with google: %v", err)
			}

			if resp.Code == http.StatusCreated {
				respSignIn, err := gah.apiClient.SignIn("", token.AccessToken, AuthType_Google)
				if err != nil {
					log.Printf("Error on signing in with Google: %v", err)
				}

				if respSignIn.Code == http.StatusOK {
					ses, err := gah.cookieStore.Get(r, sessionName)
					if err != nil {
						return
					}

					ses.Values["authorization"] = token.AccessToken
					ses.Values["user_id"] = respSignIn.UserID
					ses.Values["auth_type"] = models.AuthTypeHeader_Google

					err = ses.Save(r, w)
					if err != nil {
						return
					}

					http.Redirect(w, r, "/profile/edit", http.StatusSeeOther)
				}
			}

		} else if state == stateSignin {
			resp, err := gah.apiClient.SignIn("", token.AccessToken, AuthType_Google)
			if err != nil {
				log.Printf("Error on signing in with Google: %v", err)
			}

			if resp.Code == http.StatusOK {
				ses, err := gah.cookieStore.Get(r, sessionName)
				if err != nil {
					return
				}

				ses.Values["authorization"] = token.AccessToken
				ses.Values["user_id"] = resp.UserID
				ses.Values["auth_type"] = models.AuthTypeHeader_Google

				err = ses.Save(r, w)
				if err != nil {
					return
				}

				http.Redirect(w, r, "/profile", http.StatusSeeOther)
			}
		}

	}
}