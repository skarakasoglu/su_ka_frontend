package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang-challenge/frontend/models"
	"golang-challenge/frontend/server/requests"
	"html/template"
	"log"
	"net/http"
)

type AuthType int

const (
	AuthType_App AuthType = 1 + iota
	AuthType_Google
)

var (
	defaultURL = "/"
	sessionName = "challenge"
	sessionSecret = "test"
)

type Server struct{
	address     string
	cookieStore *sessions.CookieStore
	apiClient *apiClient
	googleAuth *googleAuthHandler
	viewPath string
	staticFilePath string
}

func NewServer(config Config, googleConfig GoogleAuthConfig) *Server{
	client := NewApiClient(config.ApiURL)
	cookieStore := sessions.NewCookieStore([]byte(sessionSecret))

	return &Server{
		address:     config.Address,
		cookieStore: cookieStore,
		googleAuth: NewGoogleAuthHandler(googleConfig, cookieStore, client),
		apiClient: client,
		viewPath: config.ViewPath,
		staticFilePath: config.StaticFilePath,
	}
}

func (srv *Server) Start() error {

	r := mux.NewRouter()

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(srv.staticFilePath))))
	r.HandleFunc("/", srv.login).Methods(http.MethodGet)
	r.HandleFunc("/login", srv.login).Methods(http.MethodGet)
	r.HandleFunc("/login", srv.loginPost).Methods(http.MethodPost)
	r.HandleFunc("/signup", srv.signup).Methods(http.MethodGet)
	r.HandleFunc("/signup", srv.signupPost).Methods(http.MethodPost)
	r.HandleFunc("/logout", srv.logout).Methods(http.MethodGet)

	passwordRoute := r.PathPrefix("/passwords").Subrouter()
	passwordRoute.HandleFunc("/forgot", srv.forgotPassword).Methods(http.MethodGet)
	passwordRoute.HandleFunc("/forgot", srv.forgotPasswordPost).Methods(http.MethodPost)
	passwordRoute.HandleFunc("/reset", srv.resetPassword).Methods(http.MethodGet)
	passwordRoute.HandleFunc("/reset", srv.resetPasswordPost).Methods(http.MethodPost)

	profileRoute := r.PathPrefix("/profile").Subrouter()
	profileRoute.HandleFunc("", srv.showProfile).Methods(http.MethodGet, http.MethodPost)
	profileRoute.HandleFunc("/edit", srv.editProfile).Methods(http.MethodGet)
	profileRoute.HandleFunc("/save", srv.saveProfile).Methods(http.MethodPost)

	googleRoute := r.PathPrefix("/google").Subrouter()
	googleRoute.HandleFunc("/signin", srv.googleAuth.SignInWithGoogle)
	googleRoute.HandleFunc("/auth", srv.googleAuth.SignInCallback)

	log.Fatal(http.ListenAndServe(srv.address, r))

	return nil
}

func (srv *Server) signup(w http.ResponseWriter, r *http.Request) {
	_, err := srv.getAuthInfo(r)

	if err == nil {
		log.Printf("Token value is found. Redirecting...")
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	signupView := fmt.Sprintf("%v/signup.gohtml", srv.viewPath)
	t, err := template.ParseFiles(signupView)
	if err != nil {
		log.Printf("Error on parsing template file: %v", err)
	}

	err = t.Execute(w, models.Base{})
	if err != nil {
		log.Printf("Error on executing the template file: %v", err)
	}
}

func (srv *Server) signupPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Printf("Error on parsing the form: %v", err)
		return
	}

	request := requests.SignUpRequest{
		Username: r.PostFormValue("username"),
		Password: r.PostFormValue("password"),
		AuthType: 2,
	}

	response, err := srv.apiClient.SignUp(r.PostFormValue("username"), r.PostFormValue("password"), 1)
	if err != nil {
		log.Printf("Error on sign up: %v", err)
		return
	}

	if response.Code != http.StatusCreated {
		log.Printf("Error on making the request: %v", response.ErrorMessage)

		signupView := fmt.Sprintf("%v/signup.gohtml", srv.viewPath)
		t, err := template.ParseFiles(signupView)
		if err != nil {
			log.Printf("Error on parsing the template file:  %v", err)
			return
		}

		err = t.Execute(w, models.Base{ErrorMessage: response.ErrorMessage})
		if err != nil {
			log.Printf("Error on executing the template file: %v", err)
			return
		}
	} else {
		resp, err := srv.apiClient.SignIn(request.Username, request.Password, AuthType_App)
		if err != nil {
			log.Printf("Error on sending login request: %v", err)
			return
		}

		err = srv.setSessionInfo(w, r, resp.AccessToken, resp.UserID)
		if err != nil {
			log.Printf("Error on setting session info: %v", err)
			return
		}

		srv.editProfile(w, r)
	}
}

func (srv *Server) login(w http.ResponseWriter, r *http.Request) {
	_, err := srv.getAuthInfo(r)

	if err == nil {
		log.Printf("Token value is found. Redirecting...")
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	t, err := template.ParseFiles(fmt.Sprintf("%v/login.gohtml", srv.viewPath))
	if err != nil {
		log.Printf("Error on parsing template file: %v", err)
		return
	}

	err = t.Execute(w, nil)
	if err != nil {
		log.Printf("Error on executing the template file: %v", err)
	}
}

func (srv *Server) loginPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Printf("Error on parsing the form: %v", err)
		return
	}

	response, err := srv.apiClient.SignIn(r.PostFormValue("username"), r.PostFormValue("password"), AuthType_App)
	if err != nil {
		log.Printf("Error on sending login request: %v", err)
		return
	}

	var model models.Base
	if response.Code != http.StatusOK {
		model.ErrorMessage = response.ErrorMessage

		viewFilePath := fmt.Sprintf("%v/login.gohtml", srv.viewPath)
		t, err := template.ParseFiles(viewFilePath)
		if err != nil {
			log.Printf("Error on parsing the template file: %v", err)
			return
		}

		 err = t.Execute(w, model)
		 if err != nil {
		 	log.Printf("Error on executing the template: %v", err)
			 return
		 }
	} else{
		err = srv.setSessionInfo(w, r, response.AccessToken, response.UserID)
		if err != nil {
			log.Printf("Error on setting session info: %v", err)
			return
		}

		http.Redirect(w, r, "/profile", http.StatusSeeOther)
	}
}

func (srv *Server) forgotPassword(w http.ResponseWriter, r *http.Request) {
	forgotPasswordFile := fmt.Sprintf("%v/forgot_password.gohtml", srv.viewPath)
	t, err := template.ParseFiles(forgotPasswordFile)
	if err != nil {
		log.Printf("Error on parsing the template file: %v", err)
		return
	}

	err = t.Execute(w, nil)
	if err != nil {
		log.Printf("Error on executing the template file: %v", err)
	}
}

func (srv *Server) forgotPasswordPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Printf("Error on parsing the form: %v", err)
		return
	}

	username := r.PostFormValue("username")

	response, err := srv.apiClient.ForgotPassword(username)
	if err != nil {
		log.Printf("Error on forgot password: %v", err)
		return
	}

	forgotPasswordView := fmt.Sprintf("%v/forgot_password.gohtml", srv.viewPath)
	t, err := template.ParseFiles(forgotPasswordView)
	if err != nil {
		log.Printf("Error on parsing the template file: %v", err)
		return
	}

	err = t.Execute(w, models.Base{
		ErrorMessage:   response.ErrorMessage,
		SuccessMessage: response.SuccessMessage,
	})
	if err != nil {
		log.Printf("Error on executing the template file: %v", err)
	}
}

func (srv *Server) resetPassword(w http.ResponseWriter, r *http.Request) {
	resetPasswordView := fmt.Sprintf("%v/reset_password.gohtml", srv.viewPath)
	t, err := template.ParseFiles(resetPasswordView)
	if err != nil {
		log.Printf("Error on parsing the template file: %v", err)
		return
	}

	username := r.URL.Query().Get("username")
	token := r.URL.Query().Get("token")
	err = t.Execute(w, models.ResetPassword{
		Username: username,
		Token:    token,
	})
	if err != nil {
		log.Printf("Error on executing the template file: %v", err)
	}
}

func (srv *Server) resetPasswordPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Printf("Error on parsing the form: %v", err)
		return
	}

	username := r.URL.Query().Get("username")
	token := r.URL.Query().Get("token")
	password := r.PostFormValue("password")

	response, err := srv.apiClient.ResetPassword(username, token, password)
	if err != nil {
		log.Printf("Error on reset password: %V", err)
		return
	}

	if response.Code != http.StatusOK {
		model := models.ResetPassword{
			Base:  models.Base{
				ErrorMessage:   response.ErrorMessage,
			},
		}

		resetPasswordView := fmt.Sprintf("%v/reset_password.gohtml", srv.viewPath)
		t, err := template.ParseFiles(resetPasswordView)
		if err != nil {
			log.Printf("Error on parsing the template file: %v", err)
			return
		}

		err = t.Execute(w, model)
		if err != nil {
			log.Printf("Error on executing the template file: %v", err)
		}
	} else {
		srv.login(w, r)
	}

}

func (srv *Server) showProfile(w http.ResponseWriter, r *http.Request) {
	authInfo, err := srv.getAuthInfo(r)
	if err != nil {
		log.Printf("Error on fetching user auth info: %v", err)
		srv.logout(w, r)
		return
	}


	response, err  := srv.apiClient.GetUserInfo(authInfo)
	if err != nil {
		log.Printf("Error on fetching the user info: %v", err)
		srv.logout(w, r)
	}

	profileViewPath := fmt.Sprintf("%v/profile.gohtml", srv.viewPath)
	t, err := template.ParseFiles(profileViewPath)
	if err != nil {
		log.Printf("Error on parsing the template file: %v", err)
		srv.logout(w, r)
		return
	}

	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")
	err = t.Execute(w, models.Profile{
		ID:        response.User.ID,
		Username:  response.User.Username,
		FullName:  response.User.FullName,
		Address:   response.User.Address,
		Telephone: response.User.Telephone,
		IsAppAuth: func() bool {
			if response.User.AuthType != 1 {
				return false
			}

			return true
		}(),
	})
	if err != nil {
		log.Printf("Error on executing the template: %v", err)
		srv.logout(w, r)
		return
	}
}

func (srv *Server) editProfile(w http.ResponseWriter, r *http.Request) {
	authInfo, err := srv.getAuthInfo(r)
	if err != nil {
		log.Printf("Error on fetching auth info: %v", err)
		srv.logout(w, r)
		return
	}

	response, err := srv.apiClient.GetUserInfo(authInfo)
	if err != nil {
		log.Printf("Error on fetching the user info: %v", err)
		srv.logout(w, r)
		return
	}

	srv.renderEditProfile(w, r, models.Profile{
		ID:        response.User.ID,
		Username:  response.User.Username,
		FullName:  response.User.FullName,
		Address:   response.User.Address,
		Telephone: response.User.Telephone,
		IsAppAuth: func() bool {
			if response.User.AuthType != 1 {
				return false
			}

			return true
		}(),
	})
}

func (srv *Server) saveProfile(w  http.ResponseWriter, r *http.Request) {
	authInfo, err  := srv.getAuthInfo(r)
	if err != nil {
		log.Printf("Error on fetching the auth info: %v", err)
		srv.logout(w, r)
		return
	}

	err = r.ParseForm()
	if err != nil {
		log.Printf("Error on parsing the form: %v", err)
		srv.logout(w, r)
		return
	}

	response, err := srv.apiClient.EditUser(authInfo.Type, authInfo.Token, authInfo.UserID,
		r.PostFormValue("username"), r.PostFormValue("full_name"),
		r.PostFormValue("address"), r.PostFormValue("telephone"))
	if err != nil {
		log.Printf("Error on editing user: %v", err)
		return
	}

	if response.Code != http.StatusOK {
		log.Printf("Error on editing the user profile. Code: %v", response.Code)

		resp, err := srv.apiClient.GetUserInfo(authInfo)
		if err != nil {
			log.Printf("Error on fetching user info: %v", err)
		}

		srv.renderEditProfile(w, r, models.Profile{
			ID:        resp.User.ID,
			Username:  resp.User.Username,
			FullName:  resp.User.FullName,
			Address:   resp.User.Address,
			Telephone: resp.User.Telephone,
			ErrorMessage: response.ErrorMessage,
			IsAppAuth: func() bool {
				if resp.User.AuthType != 1 {
					return false
				}

				return true
			}(),
		})
		return
	}

	r.Method = http.MethodGet
	http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
}

func (srv *Server) logout(w http.ResponseWriter, r *http.Request) {
	ses, err := srv.cookieStore.Get(r, sessionName)
	if err != nil {
		log.Printf("Error on fetching the cookieStore: %v", err)
		return
	}

	ses.Options.MaxAge = -1
	err = ses.Save(r, w)
	if err != nil {
		log.Printf("Error on saving the cookieStore:  %v", err)
		return
	}

	http.Redirect(w, r, defaultURL, http.StatusSeeOther)
}

func (srv *Server) getAuthInfo(r *http.Request) (models.AuthInfo, error) {
	var authInfo models.AuthInfo

	ses, err := srv.cookieStore.Get(r, sessionName)
	if err != nil {
		log.Printf("Error on getting the cookieStore: %v", err)
		return authInfo, err
	}

	token, ok := ses.Values["authorization"].(string)
	if !ok {
		return authInfo, fmt.Errorf("malformed access token")
	}
	authInfo.Token = token

	userID, ok := ses.Values["user_id"].(int)
	if !ok {
		return authInfo, fmt.Errorf("malformed user id")
	}
	authInfo.UserID = userID

	tokenType, ok := ses.Values["auth_type"].(string)
	if !ok {
		return authInfo, fmt.Errorf("malformed auth type")
	}
	authInfo.Type = models.AuthTypeHeader(tokenType)
	log.Println(authInfo)

	return authInfo, nil
}

func (srv *Server) setSessionInfo(w http.ResponseWriter, r *http.Request, token string, userID int) error {
	ses, err := srv.cookieStore.Get(r, sessionName)
	if err != nil {
		return err
	}

	ses.Values["authorization"] = token
	ses.Values["user_id"] = userID
	ses.Values["auth_type"] = string(models.AuthTypeHeader_App)

	err = ses.Save(r, w)
	if err != nil {
		return err
	}

	return nil
}

func (srv *Server) renderEditProfile(w http.ResponseWriter, r *http.Request, model models.Profile) {
	editProfileView := fmt.Sprintf("%v/edit_profile.gohtml", srv.viewPath)
	t, err := template.ParseFiles(editProfileView)
	if err != nil {
		log.Printf("Error on parsing the template file: %v", err)
		srv.logout(w, r)
		return
	}

	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")
	err = t.Execute(w, model)
	if err != nil {
		log.Printf("Error on executing the template file: %v", err)
		srv.logout(w, r)
		return
	}
}