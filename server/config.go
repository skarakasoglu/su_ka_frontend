package server

type Config struct{
	Address string
	ViewPath string
	StaticFilePath string
	ApiURL string
}
