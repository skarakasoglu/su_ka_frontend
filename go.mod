module golang-challenge/frontend

// +heroku goVersion go1.15
go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5
)
